package chayut.me.sensorous;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private static String TAG = "MainActivity";
    private SensorManager mSensorManager;
    private Sensor mSensor;

    /** data points */
    private int logSize = 100;
    private float aLight[] =  new float[logSize] ;
    private float aPressure[] =  new float[logSize];
    private float aAcc[] =  new float[logSize];

    /** view declaration */
    private GraphView graphLight,graphPressure,graphAcc;
    LineGraphSeries<DataPoint> seriesLight,seriesPressure,seriesAcc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        List<Sensor> list =  mSensorManager.getSensorList(Sensor.TYPE_ALL);
        for(Sensor sensor: list){

            Log.d(TAG,sensor.getName() +":"+ sensor.getVersion()+":"+ sensor.getVendor());
        }

        int delay = SensorManager.SENSOR_DELAY_UI;
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Log.d(TAG, "Accelerometer: " + mSensor.getName() +":"+ mSensor.getVersion()+":"+ mSensor.getVendor());
        mSensorManager.registerListener(this, mSensor, delay);

        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        if (mSensor != null) {
            Log.d(TAG, "Pressure: " + mSensor.getName() + ":" + mSensor.getVersion() + ":" + mSensor.getVendor());
            mSensorManager.registerListener(this, mSensor, delay);
        }

        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        if (mSensor != null) {
            Log.d(TAG, "Pressure: " + mSensor.getName() + ":" + mSensor.getVersion() + ":" + mSensor.getVendor());
            mSensorManager.registerListener(this, mSensor, delay);
        }


        //Test Graph
        graphLight = (GraphView) findViewById(R.id.graphLight);
        seriesLight = new LineGraphSeries<>(new DataPoint[] {
                new DataPoint(0, 1),

        });
        graphLight.getViewport().setXAxisBoundsManual(true);
        graphLight.getViewport().setMaxX(logSize);
        graphLight.getViewport().setMinX(0);
        graphLight.addSeries(seriesLight);

        graphPressure = (GraphView) findViewById(R.id.graphPressure);
        seriesPressure = new LineGraphSeries<>(new DataPoint[] {new DataPoint(0, 1),});
        graphPressure.addSeries(seriesPressure);
        graphPressure.getViewport().setXAxisBoundsManual(true);
        graphPressure.getViewport().setMaxX(logSize);
        graphPressure.getViewport().setMinX(0);

        graphAcc = (GraphView) findViewById(R.id.graphAcc);
        seriesAcc = new LineGraphSeries<>(new DataPoint[] {new DataPoint(0, 1),});
        graphAcc.addSeries(seriesAcc);
        graphAcc.getViewport().setXAxisBoundsManual(true);
        graphAcc.getViewport().setMaxX(logSize);
        graphAcc.getViewport().setMinX(0);




    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            getAccelerometer(event);
        }
        else if (event.sensor.getType() == Sensor.TYPE_PRESSURE) {
            getPressure(event);
        }
        else if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            getLight(event);
        }


    }

    private void getAccelerometer(SensorEvent event) {
        float[] values = event.values;
        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];

        float accelationSquareRoot = (x * x + y * y + z * z)
                / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
        Log.d(TAG,"ACC:" + accelationSquareRoot );

        System.arraycopy(aAcc, 1, aAcc, 0, logSize - 1);
        aAcc[logSize-1] = accelationSquareRoot;


        DataPoint[] data = new DataPoint[logSize];
        for(int i=0; i<logSize;i++){
            data[i] = new DataPoint(i, aAcc[i]);
        }
        seriesAcc.resetData(data);

    }

    private void getPressure(SensorEvent event)
    {
        float[] values = event.values;

        float pressure = values[0];
        Log.d(TAG,"Pressure:" + pressure );

        System.arraycopy(aPressure, 1, aPressure, 0, logSize - 1);
        aPressure[logSize-1] = pressure;


        DataPoint[] data = new DataPoint[logSize];
        for(int i=0; i<logSize;i++){
            data[i] = new DataPoint(i, aPressure[i]);
        }
        seriesPressure.resetData(data);

    }
    private void getLight(SensorEvent event)
    {
        float[] values = event.values;

        float light = values[0];
        Log.d(TAG, "Light:" + light);

        System.arraycopy(aLight, 1, aLight, 0, logSize - 1);
        aLight[logSize-1] = light;


        DataPoint[] data = new DataPoint[logSize];
        for(int i=0; i<logSize;i++){
            data[i] = new DataPoint(i, aLight[i]);
        }
        seriesLight.resetData(data);

    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(TAG,"onAccuracyChanged()");
    }



}
